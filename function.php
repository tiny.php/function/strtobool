<?php
/**
 * function.php
 * /
 * 
 * @package tinyphp-function/strtobool
 * @author Jesús Rojas <jrojash@jys.pe>
 * @copyright 2024, J&S Perú <https://jys.pe>
 * @created 2024-11-15 19:32:41
 * @version 20241115194101 (Rev. 9)
 * @license MIT
 * @filesource
 * 
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */


if (!function_exists('strtobool'))
{
    /**
     * Convierte una variable de cualquier tipo a un booleano
     *
     * @param mixed $val
     * @param boolean $def
     * @return boolean
     */
    function strtobool(mixed $val, bool $def = false): bool
    {
        if (is_bool($val))
            return $val;

        if (is_empty($val))
            return $def;

        $str = (string) $val;

        if (preg_match('/^(s|y|v|t|1)/i', $str))
            return true;


        if (preg_match('/^(n|f|0)/i', $str))
            return false;

        return !$def;
    }
}
