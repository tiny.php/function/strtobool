# Función strtobool

## Instalación via composer

```bin
composer require tinyphp-function/strtobool
```

## Funciones

```php
/** Convierte una variable de cualquier tipo a un booleano */
function strtobool(mixed $val, bool $def = false): bool
```

## Casos

```php
$true = true;
strtobool($true); ## true

$false = false;
strtobool($false); ## false

$null = null;
strtobool($null); ## false

$empty = '';
strtobool($empty); ## false

$empty   = '';
$default = true;
strtobool($empty, $default); ## true

$str_yes = 'yes';
strtobool($str_yes); ## true

$str_si = 'si';
strtobool($str_si); ## true

$str_true = 'true';
strtobool($str_true); ## true

$str_verdadero = 'verdadero';
strtobool($str_verdadero); ## true

$str_1 = '1';
strtobool($str_1); ## true

$int_1 = 1;
strtobool($int_1); ## true

$str_no = 'no';
strtobool($str_no); ## false

$str_false = 'false';
strtobool($str_false); ## false

$str_falso = 'falso';
strtobool($str_falso); ## false

$str_0 = '0';
strtobool($str_0); ## false

$int_0 = 0;
strtobool($int_0); ## false
```
